#!/bin/bash
	
#
# make zip file and template README for this project
# 

date=$(date --iso-8601)
base="APOLLO_splice_plates"
	
orig=splice_plates

mkdir -p fab
rm fab/*

#board info
mv ${orig}-Edge_Cuts.gbr      fab/${base}-EdgeCuts.pho
#mv ${orig}-Dwgs_User.gbr      fab/${base}-Fab_dwg.pho
mv ${orig}-User_Comments.gbr  fab/${base}-User_Comments.pho
mv ${orig}-User_Drawings.gbr  fab/${base}-User_Drawings.pho


#masks
mv ${orig}-F_Mask.gbr         fab/${base}-F_Mask.pho	
mv ${orig}-B_Mask.gbr         fab/${base}-B_Mask.pho	


#drill
mv ${orig}-PTH.drl            fab/${base}-PTH.drl	
mv ${orig}-PTH-drl_map.gbr    fab/${base}-PTH-drl_map.pho	
mv ${orig}-NPTH.drl           fab/${base}-NPTH.drl
mv ${orig}-NPTH-drl_map.gbr   fab/${base}-NPTH-drl_map.pho


#zip files
zip ${base}-${date}.zip fab/*
